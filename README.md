# How to create a sandbox?

## Dockersize your frontend

1. Create a react app here:-

```
create-react-app frontend
```

2. Create a dockerfile inside your newly created react app:-

```
touch Dockerfile
```

3. Add following content to this file:-

```
FROM node:16.15.1

WORKDIR /app

COPY package*.json ./
COPY yarn.lock ./
RUN yarn

COPY . .

ENV PORT 80

CMD ["yarn", "start"]
```

4. Create another file called `.dockerignore` and add following content:-

```
node_modules
npm-debug.log
build
.dockerignore
**/.git
**/.DS_Store
**/node_modules

```

5. Install the dependencies using following command:-

```
yarn install
```

5. Run this command to build the docker image.
```
docker build . -t frontend
```
Note that you can name the image anything you want. Here -t means a name/tag.

6. To run this image run following:-
```
docker run -it -p 3001:3000 frontend
```
Now, if you open localhost:3001 in your browser, you should be able to see the react app running.

## Dockerize your backend?

1. Change back to parent directory, where we will create a new project in express. To do so, run the following command:-
```
express Backend
```

2. Since we're using express app here, we will install nodemon to enable hot reloading using following command:-
```
npm i nodemon
```

3. Change your start command inside ```package.json``` with following command:-
```
"start": "nodemon app.js"
```

2. Move inside `/Backend` repo and create a new `Dockerfile` with following content:-

```
FROM node:16.15.1

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --omit=dev

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "node", "app.js" ]
```

9. Also create a `.dockerignore` to tell docker what to ignore while building the image:-

```
node_modules
npm-debug.log
```

10. Now build the docker image for Backend.

```
docker build . -t backend
```

6. To run this image run following:-
```
docker run -it -p 8001:8080 backend
```

## Adding your services as ```git submodules``` to your sandbox

1. Now, create a new repo called sandbox and add Frontend and Backend as git submodules using following:-

```
git submodule add https://gitlab.com/gagan.smarter.codes/backend.git Backend
git submodule add https://gitlab.com/gagan.smarter.codes/Frontend.git Frontend
```

After using the command, your sandbox should have following structure:-

```
sandbox
| - Backend
| - Frontend
```

## Adding your dockerize services in `docker-compose.yaml` in sandbox.

1. Inside sandbox, create a file called ```docker-compose.yaml``` with following content:-
```

version: "1.0"

services:

  traefik:
    image: traefik:v1.7-alpine
    container_name: traefik
    logging:
      options:
        max-size: "50m"
        max-file: "3"
    command: >
      --logLevel='INFO' --web --InsecureSkipVerify=true --defaultentrypoints=https,http --entryPoints='Name:http Address::80' --entryPoints='Name:https Address::443 TLS Redirect.EntryPoint:http' --retry --docker --docker.endpoint='unix:///var/run/docker.sock' --docker.exposedbydefault=true --docker.watch=true
    ports:
      - "80:80"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    labels:
      - "traefik.port=8080"
      - "traefik.frontend.rule=Host:localhost"
      - "traefik.protocol=http"
    networks:
      - docker_net

  backend:
    container_name: backend
    logging:
      options: 
        max-size: "50m"
        max-file: "3"
    labels:
      - "traefik.port=8080"
      - "traefik.frontend.rule=Host:backend.localhost"
      - "traefik.protocol=http"
    build:
      context: ./Backend
      dockerfile: Dockerfile
    restart: always
    command: ["npm","run","start"]
    networks:
      - docker_net

  frontend:
    container_name: frontend
    logging:
      options: 
        max-size: "50m"
        max-file: "3"
    labels:
      - "traefik.port=3000"
      - "traefik.frontend.rule=Host:frontend.localhost"
      - "traefik.protocol=http"
    build:
      context: ./Frontend
      dockerfile: Dockerfile
    command: ["npm","run","start"]
    networks:
      - docker_net

networks:
  docker_net:
    name: docker_net
    driver: bridge
```
Here we are creating two images called ```backend``` and ```frontend```. ```backend``` will use ```Dockerfile``` present at ```./Backend``` and will run at ```backend.localhost```. Similarly, ```frontend``` will use ```Dockerfile``` present at ```./Frontend``` running at ```frontend.localhost```.

2. You can start the sandbox using following command:-

```
docker compose up --build -d
```
Whenever you made any change to ```docker-compose.yaml``` file, you need to run the command.


## How to enable Hot reloading in frontend?

Hot reloading is enabled by using docker volumes. To know more about them please refer to [Official Documentation](https://docs.docker.com/storage/volumes/).

We will create a volume inside docker images. The volume will hold a copy of our existing code. 

The whole working can be summarized in following steps:-
1. Whenever you make change to your code, the change is propagated to the copy of code in volume.
2. Docker then reloads the whole project making your changes live.

In frontend, you can add volumes by adding these lines to ```docker-compose.yaml```:-

```
frontend:
    container_name: frontend
    logging:
      options: 
        max-size: "50m"
        max-file: "3"
    labels:
      - "traefik.port=3000"
      - "traefik.frontend.rule=Host:frontend.localhost"
      - "traefik.protocol=http"
    build:
      context: ./Frontend
      dockerfile: Dockerfile
    volumes:
      - "./Frontend/src:/app/src"
    command: ["npm","run","start"]
    networks:
      - docker_net
```

Now, if you restart your sandbox, your frontend should have hot reloading enabled.


> In case if you are working in a restricted environment, you will need to add ```WATCHPACK_POLLING: "true"``` to your frontend as follows:-
```
frontend:
    container_name: frontend
    logging:
      options: 
        max-size: "50m"
        max-file: "3"
    labels:
      - "traefik.port=3000"
      - "traefik.frontend.rule=Host:frontend.localhost"
      - "traefik.protocol=http"
    build:
      context: ./Frontend
      dockerfile: Dockerfile
    volumes:
      - "./Frontend/src:/app/src"
    environment:
      WATCHPACK_POLLING: "true"
    command: ["npm","run","start"]
    networks:
      - docker_net

```

## How to enable hot reloading in backend?

Similar to frontend, we can add volume to our backend as follows:-

```
backend:
    container_name: backend
    logging:
      options: 
        max-size: "50m"
        max-file: "3"
    labels:
      - "traefik.port=8080"
      - "traefik.frontend.rule=Host:backend.localhost"
      - "traefik.protocol=http"
    build:
      context: ./Backend
      dockerfile: Dockerfile
    restart: always
    command: ["npm","run","start"]
    networks:
      - docker_net
    volumes:
      - "./Backend:/usr/src/app/"
      - "/usr/src/app/node_modules"
```
You can restart your sanbox to test the live reloading.

> If you are working in a restricted environment, you will need to run nodemon in legacy mode. To do so, modify your ```package.json``` with following command:-
```
"start": "nodemon app.js -L",
```
## How to attach a debugger to your services?

Attaching a debugger is dependent on which services you are using. Since our backend is in express, we will be using express-specific instructions.

1. First, open your ```app.js``` and enter following code:-
```
var inspector = require('inspector');

inspector.open();
```

2. We will not add configuration inside ```package.json``` using following:-

```
"configurations": [
    {
      "type": "node",
      "request": "attach",
      "name": "Node: Nodemon",
      "processId": "${command:PickProcess}",
      "restart": true,
      "protocol": "inspector"
    }
],
```

3. To run a debugger, modify your start command inside ```package.json``` with following:-

```
"start": "nodemon app.js -L --inspect=0.0.0.0:9229"
```
We are telling to start a debugger using ```--inspect``` flag along with the address at which it will start.

Now, you can restart the sandbox and check the logs. You should see similar logs:-

```
[nodemon] starting `node app.js --inspect=0.0.0.0:9229`
Debugger listening on ws://127.0.0.1:9229/d88b7773-240a-445e-bfc2-66635d50f6e8
For help, see: https://nodejs.org/en/docs/inspector
Example app listening on port8080!
```